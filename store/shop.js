import * as firebase from 'firebase'
import firebaseConf from '../plugins/firebase'
import 'firebase/firestore'

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConf)
}

// Initialize Cloud Firestore through Firebase
const db = firebase.firestore();

export const state = () => ({
  products: [],
  categories: [],
  singleProduct: null,
  productsInCart: []
})

export const getters = {
  allProducts (state) {
    return state.products
  },
  allCategories (state) {
    return state.categories
  },
  singlePRODUCT (state) {
    return state.singleProduct
  },
  productsInCART (state) {
    return state.productsInCart
  }
}

export const mutations = {
  GET_PRODUCTS (state, payload) {
    let products = []
    payload.forEach(s => {
      const data = s.data()
      let product = {
        name: data.name,
        description: data.description,
        id: data.id,
        img: data.img,
        price: data.price,
        quantity: data.quantity
      }
      products.push(product)
      products.reverse()
    })
    state.products = products
  },
  GET_CATEGORIES (state, payload) {
    let categories = []
    payload.forEach(s => {
      const data = s.data()
      let category = {
        name: data.name,
        description: data.description,
        id: data.id,
        img: data.img,
        products: data.products
      }
      categories.push(category)
      categories.reverse()
    })
    state.categories = categories
  },
  GET_SINGLE_PRODUCT (state, payload) {
    let product = payload.data()
    state.singleProduct = product
  },
  ADD_PRODUCT_TO_CART (state, payload) {
    state.productsInCart.push(payload)
    // let serializePayload = JSON.stringify(payload)
    // sessionStorage.setItem(payload.id, serializePayload)
  },
  REMOVE_PRODUCT_FROM_CART (state, payload) {
    let removeItem = null
    state.productsInCart.forEach((product, index)=>{
      if (product.id == payload){
        removeItem = index
      }
    })
    state.productsInCart.splice(removeItem, 1)
  },
  CART_STATE (state, payload) {
    console.log(payload)
    //console.log(JSON.parse(sessionStorage.getItem(payload.id)))
  }
}
export const actions = { 
  async GET_PRODUCTS ({commit}, payload) {
    await db.collection('products').get()
    .then((doc) => {
      commit('GET_PRODUCTS', doc)
    })
    .catch(error => {
      commit('notify/NOTIFY', {title: 'Ошибка загрузки постов', text: error, type: 'error' },{root: true})
    })
  },
  async GET_CATEGORIES ({commit}, payload) {
    await db.collection('category').get()
    .then((doc) => {
      commit('GET_CATEGORIES', doc)
      commit('notify/NOTIFY', {title: 'Норм загрузки постов', text: error, type: 'success' },{root: true})
    })
    .catch(error => {
      //commit('notify/NOTIFY', {title: 'Ошибка загрузки категорий', text: error, type: 'error' },{root: true})
    })
  },
  async GET_SINGLE_PRODUCT ({commit}, payload) {
    await db.collection('products').doc(payload).get()
    .then((doc) => {
      commit('GET_SINGLE_PRODUCT', doc)
      commit('notify/NOTIFY', {title: 'Норм загрузки постов', text: error, type: 'success' },{root: true})
    })
    .catch(error => {
      //commit('notify/NOTIFY', {title: 'Ошибка загрузки категорий', text: error, type: 'error' },{root: true})
    })
  }
}