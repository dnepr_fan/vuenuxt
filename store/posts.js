import * as firebase from 'firebase'
import firebaseConf from '../plugins/firebase'
import 'firebase/firestore'

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConf)
}

// Initialize Cloud Firestore through Firebase
const db = firebase.firestore();

// Disable deprecated features
db.settings({
  timestampsInSnapshots: true
});

export const state = () => ({
  posts: [],
  postsInCabinet: [],
  singlePost: {},
  comments: null
})

export const getters = {
  comments (state) {
    return state.comments
  }
}

export const mutations = {
  GET_POSTS (state, payload) {
    let posts = []
    payload.forEach(s => {
      const data = s.data()
      let post = {
        title: data.title,
        message: data.message,
        userId: data.userId,
        date: new Date(+data.date),
        doc: data.doc
      }
      posts.push(post)
      posts.reverse()
    })
    state.posts = posts
  },
  GET_POSTS_IN_CABINET (state, payload) {
    state.postsInCabinet = payload
  },
  GET_SINGLE_POST (state, payload) {
    let result= payload.data()
    state.singlePost = result
  },
  GET_COMMENTS (state, payload) {
    let comments = []
    payload.forEach(s => {
      const data = s.data()
      comments.push(data)
    })
    comments.reverse()
    if (comments.length != 0){
      state.comments = comments
    }else{
      state.comments = 0
    }
  }
}
export const actions = { 
  async GET_POSTS ({ commit }) {
    await db.collection('posts').get().then((doc) => {
      commit('GET_POSTS', doc)
    })
    .catch(error => {
      commit('notify/NOTIFY', {title: 'Ошибка загрузки постов', text: error, type: 'error' },{root: true})
    })
  },
  async GET_SINGLE_POST ({ commit }, payload) {
    await firebase.firestore().collection('posts').doc(payload).get()
    .then((doc) => {
      commit('GET_SINGLE_POST', doc)
    })
    await firebase.firestore().collection('posts').doc(payload).collection('comments').get()
    .then((doc) => {
      commit('GET_COMMENTS', doc)
    })
  },
  GET_POSTS_IN_CABINET ({ commit }) {
    db.collection('posts').get()
    .then((doc) => {
      let posts = []
      doc.forEach(s => {
        const data = s.data()
        let post = {
          title: data.title,
          message: data.message,
          userId: data.userId,
          date: new Date(+data.date),
          doc: data.doc
        }
        posts.push(post)
      })
      commit('GET_POSTS_IN_CABINET', posts)
      commit('notify/NOTIFY', {title: 'Успешно загружено', text: `${posts.length} постов`, type: 'success' },{root: true})
    })
    .catch(error => {
      commit('notify/NOTIFY', {title: 'Ошибка загрузки постов', text: error, type: 'error' },{root: true})
    })
  },
  CREATE_POST ({ commit }, payload){
    db.collection('posts').doc(payload.doc).set({
      title : payload.title,
      message: payload.message,
      userId: payload.userId,
      date: payload.date,
      doc: payload.doc
    })
    .then(() => {
      commit('notify/NOTIFY', {title: payload.author, text: 'Ваш пост опубликован!', type: 'success' },{root: true})
    })
  },
  CREATE_COMMENT ({ commit }, payload) {
    db.collection('posts').doc(payload.doc).collection('comments').doc().set({
        author: payload.author, 
        title: payload.title, 
        text: payload.text, 
        userId: payload.userId, 
        day: payload.date
    })
    .then(() => {
      commit('notify/NOTIFY', {title: 'Комментарий добавлен', type: 'success' }, {root: true})
    })
    .catch( error => {
      commit('notify/NOTIFY', {title: 'Комментарий не добавлен', text: error, type: 'error' }, {root: true})
    })
  },
  EDIT_POST ({ commit }, payload) {
    firebase.firestore().collection('posts').doc(payload.doc).update({
      message: payload.message,
      title: payload.title
    })    
    .then(function() {
      commit('notify/NOTIFY', {title: 'Запись сохранена', type: 'success' }, {root: true})
    })
    .catch(function(error) {
      commit('notify/NOTIFY', {title: 'Запись не сохранена', text: error, type: 'error' }, {root: true})
    })
  }
}