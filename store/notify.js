import Vue from 'vue'
export const mutations = {
  NOTIFY (state, payload) {
    Vue.notify({
      group: 'foo',
      title: payload.title,
      text: payload.text,
      type: payload.type,
      duration: 10000
    })
  }
}