import * as firebase from 'firebase'
import firebaseConf from '../../plugins/firebase'
import 'firebase/firestore'
import { isContext } from 'vm';

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConf)
}

// Initialize Cloud Firestore through Firebase
const db = firebase.firestore();

export const state = () => ({
  doc: '',
  img: '',
  imgProgress: 0,
  categories: [],
  step: 0
})

export const getters = {
  isDoc (state) {
    return state.doc
  },
  isImg (state) {
    return state.img
  },
  prodCategories (state) {
    return state.categories
  },
  productStep (state) {
    return state.step
  }
}

export const mutations = {
  CREATE_PRODUCT (state, payload) {
    state.doc = payload
  },
  CREATE_PRODUCT_IMG (state, payload) {
    state.img = payload
  },
  GET_CATEGORIES (state, payload) {
    state.categories = payload
  },
  STEPPER (state, payload) {
    state.step = payload
  },
  LOG (state, payload) {

  }
}
export const actions = { 
  CREATE_PRODUCT({ commit }, payload){
    db.collection('products').doc(payload.doc).set({
      name : payload.name,
      description: payload.description,
      doc: payload.doc,
      id: payload.doc,
      date: payload.date,
      price: payload.price,
      quantity: payload.quantity,
      categories: []
    })
    .then(() => {
      commit('STEPPER', 2)
      commit('CREATE_PRODUCT', payload.doc)
      commit('notify/NOTIFY', {title: 'Создана категория', text: payload.name, type: 'success' },{root: true})
    })
  },
  SET_PRODUCT_IMG ({ commit }, payload) {
    commit('SET_PROCESSING', true, {root: true})
    // Upload file and metadata to the object 'images/mountains.jpg'
    let uploadTask = firebase.storage().ref().child('product').child(payload.doc).put(payload.img);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
    function(snapshot) {
      commit('CAT_IMG_PROGRESS', snapshot)
    }, function(error) {

    // A full list of error codes is available at
    // https://firebase.google.com/docs/storage/web/handle-errors
    switch (error.code) {
      case 'storage/unauthorized':
        // User doesn't have permission to access the object
        console.log(error)
        commit('notify/NOTIFY', {title: 'Ошибка!', text: error, type: 'success' },{root: true})
        break;

      case 'storage/canceled':
        // User canceled the upload
        console.log(error)
        commit('notify/NOTIFY', {title: 'Ошибка!', text: error, type: 'success' },{root: true})
        break;

      case 'storage/unknown':
        // Unknown error occurred, inspect error.serverResponse
        console.log(error)
        commit('notify/NOTIFY', {title: 'Ошибка!', text: error, type: 'success' },{root: true})
        break;
    }
    }, function() {
      // Upload completed successfully, now we can get the download URL
      commit('STEPPER', 3)
      commit('notify/NOTIFY', {title: 'Изображение товара загружено', type: 'success' },{root: true})
      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
        //Добавляем ссылку на аватарку в базу users
        firebase.firestore().collection('products').doc(payload.doc).set({
          img: downloadURL
        }, { merge: true })
        commit('CREATE_PRODUCT_IMG', downloadURL)
        commit('SET_PROCESSING', false, {root: true})
        
      })
    })
  },
  async GET_CATEGORIES ({ commit }, payload) {
    await db.collection('category').get()
    .then((doc) => {
      let categories = []
      doc.forEach(s => {
        const data = s.data()
        let category = {
          name: data.name,
          doc: data.doc
        }
        categories.push(category)
      })
      commit('GET_CATEGORIES', categories)
    })
    .catch(error => {
      commit('notify/NOTIFY', {title: 'Ошибка загрузки категорий', text: error, type: 'error' },{root: true})
    })
  },
  SET_CATEGORIES ({ commit, getters }, payload) {
    // Добавляем выбранные категории в товар, а именно в коллекцию products
    commit('LOG', payload)
    db.collection('products').doc(payload.doc).update({
      categories: payload.checkedCategories
    })
    .then(() => {
      commit('STEPPER', 4)
      commit('notify/NOTIFY', {title: 'Категории добавлены в товар', type: 'success' }, {root: true})
    })
    .catch( error => {
      commit('notify/NOTIFY', {title: 'Категории не добавлены в товар', text: error, type: 'error' }, {root: true})
    })

    // Добавляем созданный товар в выбранне категории
    payload.checkedCategories.forEach(doc => {
      commit('SET_CATEGORIES', this.getters.isDoc)
      db.collection('category').doc(doc).collection('products').doc(payload.doc).set({
        name : payload.name,
        description: payload.description,
        doc: payload.doc,
        id: payload.doc,
        date: payload.date,
        price: payload.price,
        quantity: payload.quantity,
        img: getters.isImg
      })
      .then(() => {
        commit('notify/NOTIFY', {title: 'Товар разнесён по категориям', type: 'success' }, {root: true})
      })
      .catch( error => {
        commit('notify/NOTIFY', {title: 'Товар не разнесён по категориям', text: error, type: 'error' }, {root: true})
      })
    })
  }
}